//
//  File.swift
//  deeplinkDemo
//
//  Created by Bhargav Simejiya on 02/07/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

@objc(Deeplink)
class Deeplink: NSObject {
  
  static var dlink = ""
  
  @objc func checkDeeplink() {
    Deeplink.dlink = UserDefaults.standard.string(forKey: "DEEPLINK") ?? "Please click on deeplink"
    print("DEEEEEEEEEEE", Deeplink.dlink);
  }
  
  @objc
  func getStatus(_ callback: RCTResponseSenderBlock) {
    callback([NSNull(), Deeplink.dlink])
  }
  @objc
  static func requiresMainQueueSetup() -> Bool {
    return true
  }
  
}
