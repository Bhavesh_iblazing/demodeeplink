//
//  Deeplink.m
//  deeplinkDemo
//
//  Created by Bhargav Simejiya on 02/07/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "React/RCTBridgeModule.h"

@interface RCT_EXTERN_MODULE(Deeplink, NSObject)
RCT_EXTERN_METHOD(checkDeeplink)
RCT_EXTERN_METHOD(getStatus: (RCTResponseSenderBlock)callback)
@end
