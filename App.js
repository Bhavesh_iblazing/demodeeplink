import React, { Component } from 'react';
import { NativeModules, StyleSheet, Text, View } from 'react-native';

export default class App extends Component {

  constructor() {
    super();
    console.disableYellowBox = true;
  }

  state = {
    link: 'Please wait till get deep link'
  }

  componentDidMount() {
    setTimeout(() => {
      NativeModules.Deeplink.checkDeeplink()
    }, 2500);

    setTimeout(() => {
      NativeModules.Deeplink.getStatus((error, dlink) => {
        if (dlink) {
          // We can set condition accodingly to our requirements
          if (dlink.includes('reset-password')) {
            // we can navigate to reset password screen from here
          }
          this.setState({ link: dlink });
        }
      })
    }, 3000)
  }

  render() {
    const { link } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.welcome}>{link}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});
