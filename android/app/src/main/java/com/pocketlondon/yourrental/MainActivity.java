package com.pocketlondon.yourrental;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class MainActivity extends ReactActivity {

    private static String URL;

    @Override
    protected String getMainComponentName() {
        return "deeplinkDemo";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Uri uri = getIntent().getData();
        if (uri != null) {
            Log.e("deepLink : ", "" +uri);
            URL = uri.toString();;
//            getDeeplink();
        } else {
            URL = "";
        }
    }

    public static String getMyString(){
        return URL;
    }

    public void getDeeplink() {

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;

                        if (pendingDynamicLinkData != null) {

                            deepLink = pendingDynamicLinkData.getLink();
                            URL = String.valueOf(deepLink);
                            Log.e("deepLink : ", "" +deepLink);
                            Log.e("deepLink student id : ", deepLink.getLastPathSegment());
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("","getDynamicLink:onFailure"+ e.toString());
                    }
                });
    }
}
