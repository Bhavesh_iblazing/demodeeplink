package com.pocketlondon.yourrental;

import android.util.Log;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;



public class Deeplink extends ReactContextBaseJavaModule {
    private static String isOn = "";
    public Deeplink(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void getStatus(
            Callback successCallback) {
        successCallback.invoke(null, isOn);

    }

    @ReactMethod
    public void checkDeeplink() {
        String myValue = MainActivity.getMyString();
        isOn = myValue;
    }

    @Override
    public String getName() {
        return "Deeplink";
    }

}